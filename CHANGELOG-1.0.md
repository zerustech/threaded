CHANGELOG for 1.0.x
=====================

This changelog references the relavant changes (bug and security fixes) done in
1.0 minor versions.

To get the new features in this major release, check the list at the bottom of
this file.

* 1.0.4 (2016-08-11)
    * Merge changes in 1.1 back, this is a one-off workaround.
    * Restored dev-master alias
    * Moved source code to src

* 1.0.3 (2016-08-10)
    * Removed composer.lock

* 1.0.2 (2016-07-09)
    * Fixed typo and cs issues.

* 1.0.1 (2016-07-08)
    * Added ``composer.lock``

* 1.0.0 (2016-07-09)
    * First release of the zerustech threaded component.
    * Added class ``AbstractStream``.
    * Added class ``PipedInputStream``.
    * Added class ``PipedOutputStream``.
    * Added class ``EventDispatcher``.
    * Added class ``Event``.
    * Added class ``MarkerListener``.
    * Added other basic threaded related classes and interfaces.
